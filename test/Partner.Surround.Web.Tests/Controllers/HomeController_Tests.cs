﻿using System.Threading.Tasks;
using Partner.Surround.Web.Models.TokenAuth;
using Partner.Surround.Web.Controllers;
using Shouldly;
using Xunit;

namespace Partner.Surround.Web.Tests.Controllers
{
    public class HomeController_Tests: SurroundWebTestBase
    {
        [Fact]
        public async Task Index_Test()
        {
            await AuthenticateAsync(null, new AuthenticateModel
            {
                UserNameOrEmailAddress = "admin",
                Password = "123qwe"
            });

            //Act
            var response = await GetResponseAsStringAsync(
                GetUrl<HomeController>(nameof(HomeController.Index))
            );

            //Assert
            response.ShouldNotBeNullOrEmpty();
        }
    }
}