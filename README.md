# Partner.Surround

#### 介绍
基于ABP框架封装一套MPA框架，使用Layui作为前端呈现，封装常用的功能。旨在设计快速入手，快速实践框架。  
稳定地址(老版)：http://119.3.138.127/  
测试地址(新版)：http://119.3.138.127:9527/  

#### 软件架构
基于Abp框架并接入Pear Admin前端框架，Pear Admin框架基于Layui封装，两者均开源免费。  
* Abp部分采用Mvc+分层架构，分层架构按照职责水平分层，而不是采用限界上下文垂直分层形式。  
Abp:https://github.com/aspnetboilerplate/aspnetboilerplate 
* 前端部分Mvc视图中采用三段式，顶部为css样式，中部为Html标签，底部为Js脚本,整体为Pear Admin提供的样式基础与功能。  
Pear Admin:https://gitee.com/Jmysy/Pear-Admin-Layui  

#### 部署方式
老版采用Jenkins与Docker Compose进行持续集成，利用腾讯云Coding中的制品库作为镜像存储仓库。  
新版采用Coding直接完成持续集成。  
Coding:https://coding.net/

#### 快速部署
下载镜像  
`docker pull starcity-docker.pkg.coding.net/partner.surround/imageservice/partnersurround:latest`

简化名称  
`docker tag starcity-docker.pkg.coding.net/partner.surround/imageservice/partnersurround partnersurround`

运行网站  
`docker run -dit -p 9527:80 --env ASPNETCORE_ENVIRONMENT=Development partnersurround partnersurround`

注:数据库链接使用的测试环境地址，方便直接部署运行。

#### 整体流程
![输入图片说明](https://images.gitee.com/uploads/images/2020/0705/200516_e24c4bcd_890387.png "屏幕截图.png")

#### 开发规范
《ABP开发规范》https://shimo.im/docs/JHjThwvvyRKXtYKJ/ 