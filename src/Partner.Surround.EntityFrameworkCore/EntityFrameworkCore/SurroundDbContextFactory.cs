﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Partner.Surround.Configuration;
using Partner.Surround.Web;

namespace Partner.Surround.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class SurroundDbContextFactory : IDesignTimeDbContextFactory<SurroundDbContext>
    {
        public SurroundDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<SurroundDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            SurroundDbContextConfigurer.Configure(builder, configuration.GetConnectionString(SurroundConsts.ConnectionStringName));

            return new SurroundDbContext(builder.Options);
        }
    }
}
