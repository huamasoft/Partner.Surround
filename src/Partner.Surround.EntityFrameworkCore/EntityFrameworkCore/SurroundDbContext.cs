﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using Partner.Surround.Authorization.Roles;
using Partner.Surround.Authorization.Users;
using Partner.Surround.MultiTenancy;

namespace Partner.Surround.EntityFrameworkCore
{
    public class SurroundDbContext : AbpZeroDbContext<Tenant, Role, User, SurroundDbContext>
    {
        /* Define a DbSet for each entity of the application */
        public virtual DbSet<Partner.Surround.DataDictionaries.DataDictionaryItem> DataDictionaryItem { get; set; }

        public SurroundDbContext(DbContextOptions<SurroundDbContext> options)
            : base(options)
        {
        }
    }
}
