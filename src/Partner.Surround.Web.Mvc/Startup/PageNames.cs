﻿namespace Partner.Surround.Web.Startup
{
    public class PageNames
    {
        #region GlobalPage
        public const string HomePage = "HomePage";
        #endregion

        #region SystemManagement
        public const string SystemManagement = "SystemManagement";
        public const string OrganizationUnits = "OrganizationUnits";
        public const string Users = "Users";
        public const string Roles = "Roles";
        public const string Permissions = "Permissions";
        public const string AuditLogs = "AuditLogs";
        public const string Editions = "Editions";
        public const string Tenants = "Tenants";
        public const string TenantSettings = "HostSettings";
        public const string HostSettings = "HostSettings";
        public const string Maintenance = "Maintenance";
        #endregion

        #region ResourceManagement
        public const string ResourceManagement = "ResourceManagement";
        public const string DataDictionary = "DataDictionary";
        #endregion

        #region WorkSpace
        public const string WorkSpace = "WorkSpace";
        public const string TenantConsole = "TenantConsole";
        public const string HostConsole = "HostConsole";
        #endregion
    }
}
