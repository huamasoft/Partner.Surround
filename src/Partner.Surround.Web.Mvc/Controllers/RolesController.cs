﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Partner.Surround.Web.Models.Roles;
using Partner.Surround.Authorization.Roles;
using Partner.Surround.Authorization.Roles.Dto;
using Partner.Surround.Web.Models.Common;
using System.Collections.Generic;

namespace Partner.Surround.Web.Controllers
{
    [AbpMvcAuthorize]
    public class RolesController : SurroundControllerBase
    {
        #region 初始化
        private readonly IRoleAppService _roleAppService;

        public RolesController(IRoleAppService roleAppService)
        {
            _roleAppService = roleAppService;
        }
        #endregion

        /// <summary>
        /// 首页
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 获取角色列表
        /// </summary>
        /// <returns></returns>
        public async Task<JsonResult> GetRoleList(GetPagedRoleViewModel viewModel)
        {
            var input = PagedViewModelMapToPagedInputDto<GetPagedRoleViewModel, GetPagedRoleInput>(viewModel);

            var pagedRoleList = await _roleAppService.GetPagedRole(input);

            return Json(new PagedResultViewModel<RoleDto>(pagedRoleList.TotalCount, pagedRoleList.Items));
        }

        /// <summary>
        /// 创建或更新角色
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> CreateOrUpdateRole(RoleRequestViewModel viewModel)
        {
            var getRoleForEditOutput = await _roleAppService.GetRoleForEdit(new NullableIdDto(viewModel.RoleId));

            return View(getRoleForEditOutput);
        }

        /// <summary>
        /// 创建角色
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<JsonResult> CreateRole([FromBody]CreateRoleDto input)
        {
            await _roleAppService.CreateRole(input);

            return Json(new ResponseParamViewModel(L("CreateRoleSuccessful")));
        }

        /// <summary>
        /// 更新角色
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<JsonResult> UpdateRole([FromBody]UpdateRoleDto input)
        {
            await _roleAppService.UpdateRole(input);

            return Json(new ResponseParamViewModel(L("UpdateRoleSuccessful")));
        }

        /// <summary>
        /// 删除角色
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<JsonResult> DeleteRole([FromBody]List<EntityDto<int>> input)
        {
            await _roleAppService.DeleteRole(input);

            return Json(new ResponseParamViewModel(L("DeleteRoleSuccessful")));
        }
    }
}
