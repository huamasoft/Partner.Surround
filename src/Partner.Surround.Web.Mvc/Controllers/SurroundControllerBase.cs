using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;
using Partner.Surround.CommonDto;
using Partner.Surround.Web.Models.Common;

namespace Partner.Surround.Web.Controllers
{
    public abstract class SurroundControllerBase : AbpController
    {
        protected SurroundControllerBase()
        {
            LocalizationSourceName = SurroundConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        protected SPagedInput PagedViewModelMapToPagedInputDto<TViewModel, SPagedInput>(TViewModel viewModel)
           where SPagedInput : PagedInputDto
           where TViewModel : PagedViewModel
        {
            var input = ObjectMapper.Map<SPagedInput>(viewModel);
            input.MaxResultCount = viewModel.Limit;
            input.SkipCount = (viewModel.Page - 1) * viewModel.Limit;

            return input;
        }
    }
}
