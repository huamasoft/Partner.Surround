﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Partner.Surround.Web.Models.Users;
using Partner.Surround.Authorization.Users;
using System.Collections.Generic;
using Partner.Surround.Web.Models.Common;
using Partner.Surround.Authorization.Users.Dto;

namespace Partner.Surround.Web.Controllers
{
    //[AbpMvcAuthorize]
    //public class UsersController : SurroundControllerBase
    //{
    //    private readonly IUserAppService _userAppService;

    //    public UsersController(IUserAppService userAppService)
    //    {
    //        _userAppService = userAppService;
    //    }

    //    public async Task<ActionResult> Index()
    //    {
    //        var roles = (await _userAppService.GetRoles()).Items;
    //        var model = new UserListViewModel
    //        {
    //            Roles = roles
    //        };
    //        return View(model);
    //    }

    //    public async Task<ActionResult> EditModal(long userId)
    //    {
    //        var user = await _userAppService.GetAsync(new EntityDto<long>(userId));
    //        var roles = (await _userAppService.GetRoles()).Items;
    //        var model = new EditUserModalViewModel
    //        {
    //            User = user,
    //            Roles = roles
    //        };
    //        return PartialView("_EditModal", model);
    //    }

    //    public ActionResult ChangePassword()
    //    {
    //        return View();
    //    }
    //}

    /// <summary>
    /// 用户控制器
    /// </summary>
    [AbpMvcAuthorize]
    public class UsersController : SurroundControllerBase
    {
        #region 初始化
        private readonly IUserAppService _userAppService;

        public UsersController(IUserAppService userAppService)
        {
            _userAppService = userAppService;
        }
        #endregion

        /// <summary>
        /// 首页
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 根据分页条件获取用户列表
        /// </summary>
        /// <returns></returns>
        public async Task<JsonResult> GetUserList(GetPagedUserViewModel viewModel)
        {
            var input = PagedViewModelMapToPagedInputDto<GetPagedUserViewModel, GetPagedUserInput>(viewModel);
            var pagedUserList = await _userAppService.GetPagedUser(input);

            return Json(new PagedResultViewModel<UserDto>(pagedUserList.TotalCount, pagedUserList.Items));
        }

        /// <summary>
        /// 创建或更新用户
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<ActionResult> CreateOrUpdateUser(NullableIdDto<long> input)
        {
            var userForEditOutput = await _userAppService.GetUserForEdit(input);

            return View(userForEditOutput);
        }

        /// <summary>
        /// 创建用户
        /// </summary>
        /// <param name="createOrUpdateViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<JsonResult> CreateUser([FromBody]CreateUserDto input)
        {
            await _userAppService.CreateUser(input);

            return Json(new ResponseParamViewModel(L("CreateUserSuccessful")));
        }

        /// <summary>
        /// 更新用户
        /// </summary>
        /// <param name="createOrUpdateViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<JsonResult> UpdateUser([FromBody]UpdateUserDto input)
        {
            await _userAppService.UpdateUser(input);

            return Json(new ResponseParamViewModel(L("UpdateUserSuccessful")));
        }

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<JsonResult> DeleteUser([FromBody]List<EntityDto<long>> input)
        {
            await _userAppService.DeleteUser(input);

            return Json(new ResponseParamViewModel(L("DeleteUserSuccessful")));
        }

        /// <summary>
        /// 重置密码
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<JsonResult> ResetUserPassword([FromBody]ResetPasswordInput input)
        {
            var success = await _userAppService.ResetPassword(input);
            var msg = success ? L("DeleteUserSuccessful") : L("DeleteUserFailed");
            return Json(new ResponseParamViewModel(msg));
        }
    }
}
