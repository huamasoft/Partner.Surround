﻿using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;

namespace Partner.Surround.Web.Controllers
{
    [AbpMvcAuthorize]
    public class HomeController : SurroundControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
