﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace Partner.Surround.Web.Views
{
    public abstract class SurroundViewComponent : AbpViewComponent
    {
        protected SurroundViewComponent()
        {
            LocalizationSourceName = SurroundConsts.LocalizationSourceName;
        }
    }
}
