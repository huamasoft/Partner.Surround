﻿using Abp.AspNetCore.Mvc.Views;
using Abp.Runtime.Session;
using Microsoft.AspNetCore.Mvc.Razor.Internal;

namespace Partner.Surround.Web.Views
{
    public abstract class SurroundRazorPage<TModel> : AbpRazorPage<TModel>
    {
        [RazorInject]
        public IAbpSession AbpSession { get; set; }

        protected SurroundRazorPage()
        {
            LocalizationSourceName = SurroundConsts.LocalizationSourceName;
        }
    }
}
