﻿using Abp.Application.Navigation;

namespace Partner.Surround.Web.Views.Shared.Components.SideBarMenu
{
    public class SideBarMenuViewModel
    {
        public UserMenu MainMenu { get; set; }
    }
}
