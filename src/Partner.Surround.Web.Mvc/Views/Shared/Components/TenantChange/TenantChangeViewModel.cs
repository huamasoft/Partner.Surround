﻿using Abp.AutoMapper;
using Partner.Surround.Sessions.Dto;

namespace Partner.Surround.Web.Views.Shared.Components.TenantChange
{
    [AutoMapFrom(typeof(GetCurrentLoginInformationsOutput))]
    public class TenantChangeViewModel
    {
        public TenantLoginInfoDto Tenant { get; set; }
    }
}
