﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Partner.Surround.Web.Mvc.Areas.Base.Models.DataDictionaries
{
    /// <summary>
    /// 数据字典项请求参数视图模型
    /// </summary>
    public class DataDictionaryItemRequestViewModel
    {
        /// <summary>
        /// 数据字典Id
        /// </summary>
        public int? DataDictionaryId { get; set; }

        /// <summary>
        /// 数据字典项Id
        /// </summary>
        public int? DataDictionaryItemId { get; set; }
    }
}
