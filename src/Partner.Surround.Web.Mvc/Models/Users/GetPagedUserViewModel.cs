﻿using Abp.AutoMapper;
using Partner.Surround.Authorization.Users.Dto;
using Partner.Surround.Web.Models.Common;

namespace Partner.Surround.Web.Models.Users
{
    /// <summary>
    /// 用户分页模型
    /// </summary>
    [AutoMapTo(typeof(GetPagedUserInput))]
    public class GetPagedUserViewModel : PagedViewModel
    {
        public string FilterText { get; set; }
    }
}