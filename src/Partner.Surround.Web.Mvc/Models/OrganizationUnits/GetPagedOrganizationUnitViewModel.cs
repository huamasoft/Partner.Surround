﻿using Abp.AutoMapper;
using Partner.Surround.Organizations.Dto;
using Partner.Surround.Web.Models.Common;

namespace Partner.Surround.Web.Models.OrganizationUnits
{
    /// <summary>
    /// 组织机构分页模型
    /// </summary>
    [AutoMapTo(typeof(GetPagedOrganizationUnitInput))]
    public class GetPagedOrganizationUnitViewModel : PagedViewModel
    {
        public long? Id { get; set; }

        public string DisplayName { get; set; }
    }
}
