﻿using Abp.AutoMapper;
using Partner.Surround.Authorization.Roles.Dto;
using Partner.Surround.Web.Models.Common;

namespace Partner.Surround.Web.Models.Roles
{
    /// <summary>
    /// 角色分页模型
    /// </summary>
    [AutoMapTo(typeof(GetPagedRoleInput))]
    public class GetPagedRoleViewModel : PagedViewModel
    {
        //扩展...
    }
}
