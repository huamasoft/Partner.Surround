﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Partner.Surround.Web.Models.Roles
{
    /// <summary>
    /// 角色请求视图模型
    /// </summary>
    public class RoleRequestViewModel
    {
        public int? RoleId { get; set; }
    }
}
