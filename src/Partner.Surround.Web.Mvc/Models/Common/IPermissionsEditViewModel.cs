﻿using System.Collections.Generic;
using Partner.Surround.Authorization.Roles.Dto;

namespace Partner.Surround.Web.Models.Common
{
    public interface IPermissionsEditViewModel
    {
        List<FlatPermissionDto> Permissions { get; set; }
    }
}