﻿using Abp.AutoMapper;
using Partner.Surround.Authorization.Permissions.Dto;
using Partner.Surround.Web.Models.Common;

namespace Partner.Surround.Web.Models.Permissions
{
    /// <summary>
    /// 权限分页模型
    /// </summary>
    [AutoMapTo(typeof(GetPagedPermissionInput))]
    public class GetPagedPermissionViewModel : PagedViewModel
    {
        /// <summary>
        /// 权限名称
        /// </summary>
        public string PermissionName { get; set; }
    }
}
