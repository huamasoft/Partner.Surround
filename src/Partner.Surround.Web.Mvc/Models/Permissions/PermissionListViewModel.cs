﻿using Abp.AutoMapper;
using Partner.Surround.Authorization.Permissions.Dto;

namespace Partner.Surround.Web.Models.Permissions
{
    /// <summary>
    /// 权限列表视图模型
    /// </summary>
    [AutoMap(typeof(FlatPermissionDto))]
    public class PermissionListViewModel
    {
        public string ParentName { get; set; }

        public string Name { get; set; }

        public string DisplayName { get; set; }

        public string Description { get; set; }

        public bool Checked { get; set; }
    }
}
