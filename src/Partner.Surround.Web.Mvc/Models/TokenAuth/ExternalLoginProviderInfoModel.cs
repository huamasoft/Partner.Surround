﻿using Abp.AutoMapper;
using Partner.Surround.Web.Authentication.External;

namespace Partner.Surround.Web.Models.TokenAuth
{
    [AutoMapFrom(typeof(ExternalLoginProviderInfo))]
    public class ExternalLoginProviderInfoModel
    {
        public string Name { get; set; }

        public string ClientId { get; set; }
    }
}
