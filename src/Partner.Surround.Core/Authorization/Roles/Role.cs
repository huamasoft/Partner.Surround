﻿using System.ComponentModel.DataAnnotations;
using Abp.Authorization.Roles;
using Partner.Surround.Authorization.Users;

namespace Partner.Surround.Authorization.Roles
{
    public class Role : AbpRole<User>
    {
        public const int MaxDescriptionLength = 5000;

        public Role() //For EFCore
        {
        }

        public Role(int? tenantId, string name, string displayName)
            : base(tenantId, name, displayName)
        {
        }

        public static Role CreateRole(int? tenantId, string name, string displayName)
        {
            var role = new Role(tenantId, name, displayName);
            role.SetNormalizedName();
            return role;
        }

        public Role SetDisplayName(string displayName)
        {
            DisplayName = displayName;
            return this;
        }

        public Role SetDescription(string description)
        {
            Description = description;
            return this;
        }

        public Role SetIsDefault(bool isDefault)
        {
            IsDefault = isDefault;
            return this;
        }

        [StringLength(MaxDescriptionLength)]
        public string Description { get; set; }
    }
}
