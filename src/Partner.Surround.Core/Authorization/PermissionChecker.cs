﻿using Abp.Authorization;
using Partner.Surround.Authorization.Roles;
using Partner.Surround.Authorization.Users;

namespace Partner.Surround.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
