﻿using Abp.Authorization;
using Abp.Localization;
using Abp.MultiTenancy;

namespace Partner.Surround.Authorization
{
    public class SurroundAuthorizationProvider : AuthorizationProvider
    {
        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            #region GlobalPermission
            var pages = context.CreatePermission(PermissionNames.Pages, L("Pages"));
            #endregion

            #region SystemManagement
            var systemManagement = pages.CreateChildPermission(PermissionNames.Pages_SystemManagement, L("SystemManagement"));

            var organizationUnits = systemManagement.CreateChildPermission(PermissionNames.Pages_SystemManagement_OrganizationUnits, L("OrganizationUnitManagement"));
            organizationUnits.CreateChildPermission(PermissionNames.Pages_SystemManagement_OrganizationUnits_Create, L("CreateOrganizationUnit"));
            organizationUnits.CreateChildPermission(PermissionNames.Pages_SystemManagement_OrganizationUnits_Update, L("UpdateOrganizationUnit"));
            organizationUnits.CreateChildPermission(PermissionNames.Pages_SystemManagement_OrganizationUnits_Delete, L("DeleteOrganizationUnit"));
            organizationUnits.CreateChildPermission(PermissionNames.Pages_SystemManagement_OrganizationUnits_MoveOrganizationUnit, L("MoveOrganizationUnit"));

            var users = systemManagement.CreateChildPermission(PermissionNames.Pages_SystemManagement_Users, L("UserManagement"));
            users.CreateChildPermission(PermissionNames.Pages_SystemManagement_Users_Create, L("CreateUser"));
            users.CreateChildPermission(PermissionNames.Pages_SystemManagement_Users_Update, L("UpdateUser"));
            users.CreateChildPermission(PermissionNames.Pages_SystemManagement_Users_Delete, L("DeleteUser"));
            users.CreateChildPermission(PermissionNames.Pages_SystemManagement_Users_ResetPassword, L("ResetPassword"));

            var roles = systemManagement.CreateChildPermission(PermissionNames.Pages_SystemManagement_Roles, L("RoleManagement"));
            roles.CreateChildPermission(PermissionNames.Pages_SystemManagement_Roles_Create, L("CreateRole"));
            roles.CreateChildPermission(PermissionNames.Pages_SystemManagement_Roles_Update, L("UpdateRole"));
            roles.CreateChildPermission(PermissionNames.Pages_SystemManagement_Roles_Delete, L("DeleteRole"));

            systemManagement.CreateChildPermission(PermissionNames.Pages_SystemManagement_Permissions, L("PermissionManagement"));

            systemManagement.CreateChildPermission(PermissionNames.Pages_SystemManagement_AuditLogs, L("AuditLogs"));

            var editions = systemManagement.CreateChildPermission(PermissionNames.Pages_SystemManagement_Editions, L("EditionManagement"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(PermissionNames.Pages_SystemManagement_Editions_Create, L("CreateEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(PermissionNames.Pages_SystemManagement_Editions_Update, L("UpdateEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(PermissionNames.Pages_SystemManagement_Editions_Delete, L("DeleteEdition"), multiTenancySides: MultiTenancySides.Host);

            var tenants = systemManagement.CreateChildPermission(PermissionNames.Pages_SystemManagement_Tenants, L("TenantManagement"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(PermissionNames.Pages_SystemManagement_Tenants_MoveTenantToAnotherEdition, L("MoveTenantToAnotherEdition"), multiTenancySides: MultiTenancySides.Host);

            systemManagement.CreateChildPermission(PermissionNames.Pages_SystemManagement_TenantSettings, L("TenantSettings"), multiTenancySides: MultiTenancySides.Tenant);
            systemManagement.CreateChildPermission(PermissionNames.Pages_SystemManagement_HostSettings, L("HostSettings"), multiTenancySides: MultiTenancySides.Host);

            var maintenance = systemManagement.CreateChildPermission(PermissionNames.Pages_SystemManagement_Maintenance, L("Maintenance"));
            var logs = maintenance.CreateChildPermission(PermissionNames.Pages_SystemManagement_Maintenance_Logs, L("Logs"));
            logs.CreateChildPermission(PermissionNames.Pages_SystemManagement_Maintenance_Logs_DownLoad, L("DownLoadLog"));
            logs.CreateChildPermission(PermissionNames.Pages_SystemManagement_Maintenance_Logs_Refresh, L("RefreshLog"));
            #endregion

            #region ResourceManagement
            var resourceManagement = pages.CreateChildPermission(PermissionNames.Pages_ResourceManagement, L("ResourceManagement"), multiTenancySides: MultiTenancySides.Tenant);

            var dataDictionary = resourceManagement.CreateChildPermission(PermissionNames.Pages_ResourceManagement_DataDictionary, L("DataDictionary"), multiTenancySides: MultiTenancySides.Tenant);

            var dataDictionaryItem = dataDictionary.CreateChildPermission(PermissionNames.Pages_ResourceManagement_DataDictionary_DataDictionaryItem, L("DataDictionaryItem"), multiTenancySides: MultiTenancySides.Tenant);
            dataDictionaryItem.CreateChildPermission(PermissionNames.Pages_ResourceManagement_DataDictionary_DataDictionaryItem_Create, L("CreateDataDictionaryItem"), multiTenancySides: MultiTenancySides.Tenant);
            dataDictionaryItem.CreateChildPermission(PermissionNames.Pages_ResourceManagement_DataDictionary_DataDictionaryItem_Update, L("UpdateDataDictionaryItem"), multiTenancySides: MultiTenancySides.Tenant);
            dataDictionaryItem.CreateChildPermission(PermissionNames.Pages_ResourceManagement_DataDictionary_DataDictionaryItem_Delete, L("DeleteDataDictionaryItem"), multiTenancySides: MultiTenancySides.Tenant);
            #endregion

            #region WorkSpace
            var workSpace = pages.CreateChildPermission(PermissionNames.Pages_WorkSpace, L("WorkSpace"));
            workSpace.CreateChildPermission(PermissionNames.Pages_WorkSpace_TenantConsole, L("TenantConsole"), multiTenancySides: MultiTenancySides.Tenant);
            workSpace.CreateChildPermission(PermissionNames.Pages_WorkSpace_HostConsole, L("HostConsole"), multiTenancySides: MultiTenancySides.Host);
            #endregion
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, SurroundConsts.LocalizationSourceName);
        }
    }
}
