﻿namespace Partner.Surround
{
    public class SurroundConsts
    {
        public const string LocalizationSourceName = "Surround-zh-Hans";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;

        public const bool AllowTenantsToChangeEmailSettings = false;
    }
}
