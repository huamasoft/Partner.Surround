﻿using Abp.Domain.Services;

namespace Partner.Surround
{
    public class SurroundDomainServiceBase : DomainService
    {
        protected SurroundDomainServiceBase()
        {
            LocalizationSourceName = SurroundConsts.LocalizationSourceName;
        }
    }
}
