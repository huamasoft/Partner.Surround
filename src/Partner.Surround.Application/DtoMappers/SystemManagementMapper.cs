﻿using Abp.Auditing;
using Abp.Authorization;
using Abp.Organizations;
using AutoMapper;
using Partner.Surround.Auditing.Dto;
using Partner.Surround.Authorization.Permissions.Dto;
using Partner.Surround.Authorization.Roles;
using Partner.Surround.Authorization.Roles.Dto;
using Partner.Surround.Authorization.Users;
using Partner.Surround.Authorization.Users.Dto;
using Partner.Surround.Organizations.Dto;

namespace Partner.Surround.DtoMappers
{
    internal static class SystemManagementMapper
    {
        public static void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<OrganizationUnit, OrganizationUnitDto>();
            configuration.CreateMap<AuditLog, AuditLogListDto>();
            configuration.CreateMap<Role, RoleDto>();
            configuration.CreateMap<User, UserDto>();
            configuration.CreateMap<Permission, PermissionDto>();
        }
    }
}
