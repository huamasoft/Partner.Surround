﻿using AutoMapper;
using Partner.Surround.DataDictionaries;
using Partner.Surround.DataDictionaries.Dto;

namespace Partner.Surround.DtoMappers
{
    internal static class ResourceManagementMapper
    {
        public static void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<DataDictionaryItem, DataDictionaryItemDto>();
        }
    }
}
