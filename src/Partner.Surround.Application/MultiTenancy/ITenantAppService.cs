﻿using Abp.Application.Services;
using Partner.Surround.MultiTenancy.Dto;

namespace Partner.Surround.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

