﻿using System.Collections.Generic;
using Partner.Surround.Auditing.Dto;
using Partner.Surround.CommonDto;

namespace Partner.Surround.Auditing.Exporting
{
    public interface IAuditLogListExcelExporter
    {
        FileDto ExportToFile(List<AuditLogListDto> auditLogListDtos);

        FileDto ExportToFile(List<EntityChangeListDto> entityChangeListDtos);
    }
}
