﻿using Abp.Application.Services.Dto;

namespace Partner.Surround.CommonDto
{
    /// <summary>
    /// 分页及排序Dto
    /// </summary>
    public class PagedAndSortedInputDto : PagedInputDto, ISortedResultRequest
    {
        public PagedAndSortedInputDto()
        {
            MaxResultCount = AppConsts.DefaultPageSize;
        }

        public string Sorting { get; set; }
    }
}
