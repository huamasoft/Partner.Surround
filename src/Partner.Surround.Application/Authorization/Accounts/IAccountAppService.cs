﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Partner.Surround.Authorization.Accounts.Dto;

namespace Partner.Surround.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
