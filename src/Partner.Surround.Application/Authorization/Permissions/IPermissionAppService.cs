﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Partner.Surround.Authorization.Permissions.Dto;

namespace Partner.Surround.Authorization.Permissions
{
    public interface IPermissionAppService : IApplicationService
    {
        /// <summary>
        /// 分页筛选获取权限树结构（树形结构数据）
        /// </summary>
        /// <param name="input">分页、筛选条件</param>
        /// <returns></returns>
        PagedResultDto<PermissionDto> GetPagedPermission(GetPagedPermissionInput input);

        /// <summary>
        /// 获取完整权限树结构
        /// </summary>
        /// <returns></returns>
        ListResultDto<FlatPermissionWithLevelDto> GetAllPermission();

        /// <summary>
        /// 获取权限树根节点
        /// </summary>
        /// <returns></returns>
        ListResultDto<TreePermissionDto> GetAllPermissionTree();
    }
}
