using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Authorization;

namespace Partner.Surround.Authorization.Permissions.Dto
{
    /// <summary>
    /// Ȩ��Dto
    /// </summary>
    public class PermissionDto
    {
        public string Name { get; set; }

        public string DisplayName { get; set; }

        public string Description { get; set; }
    }
}
