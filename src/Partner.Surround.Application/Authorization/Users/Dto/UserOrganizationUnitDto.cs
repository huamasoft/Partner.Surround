﻿namespace Partner.Surround.Authorization.Users.Dto
{
    /// <summary>
    /// 用户组织机构Dto
    /// </summary>
    public class UserOrganizationUnitDto
    {
        public long OrganizationUnitId { get; set; }

        public long? OrganizationUnitParentId { get; set; }

        public string OrganizationUnitCode { get; set; }

        public string DisplayName { get; set; }

        public bool IsAssigned { get; set; }
    }
}
