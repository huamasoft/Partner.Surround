using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Auditing;
using Abp.Authorization.Users;
using Abp.Runtime.Validation;

namespace Partner.Surround.Authorization.Users.Dto
{
    public class UpdateUserDto : EntityDto<long>, IShouldNormalize
    {
        [Required]
        [StringLength(AbpUserBase.MaxUserNameLength)]
        public string UserName { get; set; }

        [Required]
        [StringLength(AbpUserBase.MaxNameLength)]
        public string Name { get; set; }

        [Required]
        [StringLength(AbpUserBase.MaxSurnameLength)]
        public string Surname { get; set; }

        [Required]
        [EmailAddress]
        [StringLength(AbpUserBase.MaxEmailAddressLength)]
        public string EmailAddress { get; set; }

        public bool IsActive { get; set; }

        public string[] AssignedRoleNames { get; set; }

        public long[] AssignedOrganizationUnitIds { get; set; }

        [Required]
        [StringLength(AbpUserBase.MaxPlainPasswordLength)]
        [DisableAuditing]
        public string Password { get; set; }

        public void Normalize()
        {
            if (AssignedRoleNames == null)
            {
                AssignedRoleNames = new string[0];
            }

            if (AssignedOrganizationUnitIds == null)
            {
                AssignedOrganizationUnitIds = new long[0];
            }
        }
    }
}
