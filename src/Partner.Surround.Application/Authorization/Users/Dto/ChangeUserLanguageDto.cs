using System.ComponentModel.DataAnnotations;

namespace Partner.Surround.Authorization.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}