using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Roles;

namespace Partner.Surround.Authorization.Roles.Dto
{
    public class UpdateRoleDto : EntityDto<int>
    {
        [Required]
        [StringLength(AbpRoleBase.MaxDisplayNameLength)]
        public string DisplayName { get; set; }

        [StringLength(Role.MaxDescriptionLength)]
        public string Description { get; set; }

        public bool IsDefault { get; set; }

        [Required]
        public List<string> GrantedPermissions { get; set; }
    }
}