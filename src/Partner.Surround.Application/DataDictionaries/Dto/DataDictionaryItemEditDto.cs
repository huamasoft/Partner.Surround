﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Partner.Surround.DataDictionaries.Dto
{
    /// <summary>
    /// 数据字典项编辑Dto
    /// </summary>
    public class DataDictionaryItemEditDto : NullableIdDto<long>
    {
        /// <summary>
        /// 字典Id
        /// </summary>
        [Required]
        public long DataDictionaryId { get; set; }

        /// <summary>
        /// 业务代码
        /// </summary>
        [Required]
        public string Code { get; set; }

        /// <summary>
        /// 字典项名称
        /// </summary>
        [Required]
        public string Name { get; set; }
    }
}
