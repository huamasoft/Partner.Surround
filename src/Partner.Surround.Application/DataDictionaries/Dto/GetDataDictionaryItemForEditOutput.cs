﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Partner.Surround.DataDictionaries.Dto
{
    /// <summary>
    /// 获取数据字典项编辑响应Dto
    /// </summary>
    public class GetDataDictionaryItemForEditOutput
    {
        /// <summary>
        /// 数据字典项
        /// </summary>
        public DataDictionaryItemEditDto DataDictionaryItem { get; set; }
    }
}
