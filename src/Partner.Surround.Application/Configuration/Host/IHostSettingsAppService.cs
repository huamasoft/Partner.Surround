﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Partner.Surround.Configuration.Host.Dto;

namespace Partner.Surround.Configuration.Host
{
    public interface IHostSettingsAppService : IApplicationService
    {
        Task<HostSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(HostSettingsEditDto input);
    }
}
