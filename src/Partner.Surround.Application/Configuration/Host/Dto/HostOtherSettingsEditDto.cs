﻿namespace Partner.Surround.Configuration.Host.Dto
{
    public class HostOtherSettingsEditDto
    {
        public bool IsQuickThemeSelectEnabled { get; set; }
    }
}