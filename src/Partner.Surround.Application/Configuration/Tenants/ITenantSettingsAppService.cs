﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Partner.Surround.Configuration.Tenants.Dto;

namespace Partner.Surround.Configuration.Tenants
{
    public interface ITenantSettingsAppService : IApplicationService
    {
        Task<TenantSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(TenantSettingsEditDto input);
    }
}
