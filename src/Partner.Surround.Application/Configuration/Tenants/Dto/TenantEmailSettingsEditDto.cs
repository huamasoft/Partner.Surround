﻿using Abp.Auditing;
using Partner.Surround.Configuration.Dto;

namespace Partner.Surround.Configuration.Tenants.Dto
{
    public class TenantEmailSettingsEditDto : EmailSettingsEditDto
    {
        public bool UseHostDefaultEmailSettings { get; set; }
    }
}