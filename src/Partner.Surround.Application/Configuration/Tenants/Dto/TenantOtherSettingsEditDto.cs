namespace Partner.Surround.Configuration.Tenants.Dto
{
    public class TenantOtherSettingsEditDto
    {
        public bool IsQuickThemeSelectEnabled { get; set; }
    }
}