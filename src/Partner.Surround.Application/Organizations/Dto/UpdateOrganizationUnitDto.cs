﻿using Abp.Application.Services.Dto;

namespace Partner.Surround.Organizations.Dto
{
    public class UpdateOrganizationUnitDto : EntityDto<long>
    {
        public string DisplayName { get; set; }
    }
}
