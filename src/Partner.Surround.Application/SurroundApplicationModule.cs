﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Partner.Surround.Authorization;
using Partner.Surround.DtoMappers;

namespace Partner.Surround
{
    [DependsOn(
        typeof(SurroundCoreModule),
        typeof(AbpAutoMapperModule))]
    public class SurroundApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            // Configure Authorization
            Configuration.Authorization.Providers.Add<SurroundAuthorizationProvider>();

            // SystemManagement Mappings
            Configuration.Modules.AbpAutoMapper().Configurators.Add(SystemManagementMapper.CreateMappings);
            Configuration.Modules.AbpAutoMapper().Configurators.Add(ResourceManagementMapper.CreateMappings);
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(SurroundApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddMaps(thisAssembly)
            );
        }
    }
}
