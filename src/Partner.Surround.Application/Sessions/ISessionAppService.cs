﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Partner.Surround.Sessions.Dto;

namespace Partner.Surround.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
